import matplotlib.pyplot as plt
import numpy as np
from sklearn.decomposition import PCA


def pca():
    seeds_values = np.loadtxt("seeds.csv", skiprows=1, delimiter=",")
    seeds_types = np.loadtxt("seeds.csv", skiprows=1, delimiter=",")[:, 7]
    seeds_values = np.delete(seeds_values, 7, axis=1)
    seeds_values_means = np.mean(seeds_values, axis=0)
    seeds_values = seeds_values - seeds_values_means
    seeds_values_covs = np.cov(seeds_values.T)
    eig_val, eig_vec = np.linalg.eig(seeds_values_covs)
    eig_vec = eig_vec.T
    eig_value_sort = np.argsort(eig_val)[::-1]
    eig_val = eig_val[eig_value_sort]
    eig_vec = eig_vec[eig_value_sort]
    pc = eig_vec[0:2]
    data = np.dot(seeds_values, pc.T)
    print(
        "OWN IMPLEMENTATION: ",
        "Explained Variance: ",
        eig_val[:2],
        "Explained Variance Ratio: ",
        (eig_val[:2] / np.sum(eig_val)),
    )
    x = data[:, 0]
    y = data[:, 1]
    plt.scatter(x, y, c=seeds_types)
    plt.show()


def pca_sklearn_decomposition():
    seeds_values = np.loadtxt("seeds.csv", skiprows=1, delimiter=",")
    seeds_types = np.loadtxt("seeds.csv", skiprows=1, delimiter=",")[:, 7]
    seeds_values = np.delete(seeds_values, 7, axis=1)
    pca = PCA(n_components=2)
    pca.fit(seeds_values)
    print(
        "PCA_SKLEARN_DECOMPOSITION: ",
        "Explained Variance: ",
        pca.explained_variance_,
        "Explained Variance Ratio: ",
        pca.explained_variance_ratio_,
    )
    transform_seeds_values = pca.transform(seeds_values)
    x = transform_seeds_values[:, 0]
    y = transform_seeds_values[:, 1]
    plt.scatter(x, y, c=seeds_types)
    plt.show()


def pca_start():
    pca()
    pca_sklearn_decomposition()


if __name__ == "__main__":
    pca_start()
