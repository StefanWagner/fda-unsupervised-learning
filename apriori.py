def apriori_start():
  # Data for the algorithm.
  min_support = 18
  data = list()
  with open("books.txt") as file:
    for file_line in file:
      line = file_line.replace("\n", "")
      user_data = frozenset(line.split(";"))
      data.append(user_data)
  # First iteration.
  candidates = first(data, min_support)
  all_candidates = set()
  # Left Iterations.
  while len(candidates) != 0:
    pruned_candidates = set()
    for candidate in candidates:
      candidate_support = 0
      for user_data in data:
        if user_data.issuperset(candidate):
          candidate_support += 1
      if candidate_support >= min_support:
        pruned_candidates.add(candidate)
        if (len(candidate) > 1):
          with open("patterns.txt", "a") as file:
            file.write("\n" + str(candidate_support) + ":")
            for book in candidate:
              file.write(str(book) + ";")
          all_candidates.add(candidate)
    candidates = candidate_generation(pruned_candidates)
  # Book Recommendation.
  book_recommendation(data, all_candidates)


def first(data, min_support):
  # Get all candidates out of the data.
  candidates = set()
  for user_data in data:
    for candidate in user_data:
      candidates.add(frozenset([candidate]))
  # Get pruned candidates out of the candidates.
  pruned_candidates = set()
  for candidate in candidates:
    candidate_support = 0
    for user_data in data:
      if user_data.issuperset(candidate):
        candidate_support += 1
    if candidate_support >= min_support:
      pruned_candidates.add(candidate)
      with open("oneItems.txt", "a") as file:
        file.write(str(candidate_support) + ":" + next(iter(candidate)) + "\n")
  return pruned_candidates


def candidate_generation(pruned_candidates):
  candidates_count = len(next(iter(pruned_candidates))) + 1
  candidates = set()
  books = set()
  for old_candidate in pruned_candidates:
    for book in old_candidate:
      books.add(book)
  for old_candidate in pruned_candidates:
    for book in books:
      candidate = old_candidate.union([book])
      if len(candidate) == candidates_count:
        candidates.add(candidate)
  return candidates


def book_recommendation(data, all_candidates):
  user_liked_books = frozenset(["Harry Potter and the Sorcerers Stone (Book 1)", "Harry Potter and the Chamber of Secrets (Book 2)"])
  max_confidence = 0
  recommended_book = ""
  for candidate in all_candidates:
    if candidate.issuperset(user_liked_books) and (len(candidate) > len(user_liked_books)):
      trimmed_recommendation_candidates = candidate.difference(user_liked_books)
      for book in trimmed_recommendation_candidates:
        union_support = 0
        user_liked_books_support = 0
        union = user_liked_books.union(frozenset([book]))
        for user_data in data:
          if user_data.issuperset(union):
            union_support += 1
          if user_data.issuperset(user_liked_books):
            user_liked_books_support += 1
        confidence = union_support / user_liked_books_support
        print("Confidence: ", confidence, "Recommendation: ", book)
        if confidence > max_confidence:
          max_confidence = confidence
          recommended_book = book
  print("MAX CONFIDENCE: ", max_confidence, "RECOMMENDATION: ", recommended_book)


if __name__ == '__main__':
	apriori_start()