import numpy as np
import matplotlib.pyplot as plt
from sklearn.cluster import DBSCAN
from sklearn import metrics
from sklearn.cluster import KMeans


# Read datasets from source file
dataset = {}
dataset[0] = np.genfromtxt("dataset1_noCluster7.csv", delimiter=",", skip_header=1)
dataset[1] = np.genfromtxt("dataset2_noCluster6.csv", delimiter=",", skip_header=1)
dataset[2] = np.genfromtxt("dataset3_noCluster2.csv", delimiter=",", skip_header=1)
dataset[3] = np.genfromtxt("dataset4_noCluster2.csv", delimiter=",", skip_header=1)
dataset[4] = np.genfromtxt("dataset5_noCluster2.csv", delimiter=",", skip_header=1)

# Dataset Parameters

## DBSCAN Parameters ##
# The maximum distance between two samples
# for one to be considered as in the neighborhood of the other.
eps = [1.4, 1.5, 2.2, 0.2, 0.3]

# The number of samples (or total weight) in a neighborhood for
# a point to be considered as a core point.
min_samples = [7, 3, 27, 25, 9]

# The number of clusters to form as well as the number of centroids to generate.
n_clusters = [7, 3, 2, 8, 4]


def drawPlot(dataset):

    x = dataset[:, 0]
    y = dataset[:, 1]
    labels = dataset[:, -1]
    unique_labels = np.unique(labels)

    color_id = 0
    for l in unique_labels:
        row_filter = np.where(labels == l)[0]
        plt.scatter(x[row_filter], y[row_filter], color=colors[color_id], label=int(l))
        color_id += 1

    # https://stackoverflow.com/questions/4700614/how-to-put-the-legend-out-of-the-plot
    plt.legend(bbox_to_anchor=(0.98, 0.5), loc="upper left")
    plt.show()


def DBSCAN_clustering(dataset, eps, minSamples, print_report):

    X = dataset[:, :2]
    labels_true = dataset[:, -1]
    # https://scikit-learn.org/stable/modules/generated/sklearn.cluster.DBSCAN.html
    # https://scikit-learn.org/stable/auto_examples/cluster/plot_dbscan.html#sphx-glr-auto-examples-cluster-plot-dbscan-py
    clustering = DBSCAN(eps=eps, min_samples=minSamples).fit(X)

    core_samples_mask = np.zeros_like(clustering.labels_, dtype=bool)
    core_samples_mask[clustering.core_sample_indices_] = True
    labels = clustering.labels_

    # Number of clusters in labels, ignoring noise if present.
    n_clusters_ = len(set(labels)) - (1 if -1 in labels else 0)
    n_noise_ = list(labels).count(-1)

    if print_report:
        print("")
        print("=== DBSCAN  eps:%0.1f  min_samples:%d ===" % (eps, minSamples))
        print("Estimated number of clusters: %d" % n_clusters_)
        print("Estimated number of noise points: %d" % n_noise_)
        print(
            "Adjusted Rand Index: %0.3f"
            % metrics.adjusted_rand_score(labels_true, labels)
        )
        print(
            "Normalized Mutual Info Score: %0.3f"
            % metrics.normalized_mutual_info_score(labels_true, labels)
        )

    return metrics.normalized_mutual_info_score(labels_true, labels)


def KMeans_clustering(dataset, alg, n_clusters, print_report):

    X = dataset[:, :2]
    labels_true = dataset[:, -1]

    # https://scikit-learn.org/stable/modules/generated/sklearn.cluster.KMeans.html
    kmeans = KMeans(n_clusters=n_clusters, algorithm=alg, n_init=10, max_iter=300).fit(
        X
    )
    label = kmeans.labels_

    if print_report:
        print("")
        print("=== KMeans  algorithm:elkan  n_clusters:%d ===" % (n_clusters))
        print(
            "Adjusted Rand Index: %0.3f"
            % metrics.adjusted_rand_score(labels_true, label)
        )
        print(
            "Normalized Mutual Info Score: %0.3f"
            % metrics.normalized_mutual_info_score(labels_true, label)
        )

    return metrics.normalized_mutual_info_score(labels_true, label)


"""
labels = {}
for i in range(len(dataset)):
    labels[i] = dataset[i][:,-1]
"""

colors = [
    "green",
    "blue",
    "orange",
    "red",
    "purple",
    "yellow",
    "gray",
    "pink",
    "olive",
    "brawn",
]

for i in range(len(dataset)):

    print("")
    print("")
    print("### Dataset %d ###" % (i + 1))

    # DBSCAN Clustering
    test_mode = False
    if test_mode:
        best_eps = 0.1
        best_minSamples = 1
        n = 30  # range
        print("")
        print("TEST eps,min_samples")
        max_score = 0
        for e in range(1, n):
            ep = e / 10

            for ms in range(1, n):
                nmiscore = DBSCAN_clustering(dataset[i], ep, ms, False)
                if nmiscore > max_score:
                    max_score = nmiscore
                    best_minSamples = ms
                    best_eps = ep

        print("Best Score : %0.4f" % max_score)
        print("END TEST")

        DBSCAN_clustering(dataset[i], best_eps, best_minSamples, True)

    DBSCAN_clustering(dataset[i], eps[i], min_samples[i], True)

    # KMeans clustering
    # test_mode = True
    if test_mode:
        max_score = 0
        best_n_clusters = 1
        max_score = 0
        for n in range(2, 10):
            nmiscore = KMeans_clustering(dataset[i], "elkan", n, False)
            if nmiscore > max_score:
                max_score = nmiscore
                best_n_clusters = n

        KMeans_clustering(dataset[i], "elkan", best_n_clusters, True)

    KMeans_clustering(dataset[i], "elkan", n_clusters[i], True)

    drawPlot(dataset[i])
