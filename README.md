# FDA Unsupervised Learning

## Description

Implementation of principal component analysis from scratch for dimensionality reduction.
Usage and comparison of different clustering algorithms.
Implementation of apriori algorithm for association analysis. 

## Team

Myself

## Technology

Python, Scikit-learn Decomposition, Scikit-learn Clustering

## My Responsibility

I was responsible for everything.